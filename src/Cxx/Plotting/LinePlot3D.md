### Description

This example illustrates plotting 3D points using vtkPlotLine3D. It plots the solution to the [Lorenz Attractor](https://en.wikipedia.org/wiki/Lorenz_system).
