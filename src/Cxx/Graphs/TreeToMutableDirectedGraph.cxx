#include <vtkMutableDirectedGraph.h>
#include <vtkNew.h>
#include <vtkPoints.h>
#include <vtkTree.h>

int main(int, char*[])
{
  // vtkTree is a read-only data structure. To construct a tree, create an
  // instance of vtkMutableDirectedGraph. Add vertices and edges with
  // AddVertex() and AddEdge(). After building the tree, call
  // tree->CheckedShallowCopy(graph) to copy the structure into a vtkTree.

  // Create a graph.
  vtkNew<vtkMutableDirectedGraph> mdg;

  // Add 4 vertices to the graph.
  vtkIdType v1 = mdg->AddVertex();
  vtkIdType v2 = mdg->AddVertex();
  vtkIdType v3 = mdg->AddVertex();
  vtkIdType v4 = mdg->AddVertex();

  // Add 3 edges to the graph.
  mdg->AddEdge(v1, v2);
  mdg->AddEdge(v1, v3);
  mdg->AddEdge(v1, v4);

  // Create 4 points - one for each vertex.
  vtkNew<vtkPoints> points;
  points->InsertNextPoint(0.0, 0.0, 0.0);
  points->InsertNextPoint(1.0, 0.0, 0.0);
  points->InsertNextPoint(0.0, 1.0, 0.0);
  points->InsertNextPoint(0.0, 0.0, 2.0);

  // Add the coordinates of the points to the graph.
  mdg->SetPoints(points);

  std::cout << "MDG: " << std::endl;
  std::cout << " Type: " << mdg->GetClassName() << std::endl;
  std::cout << " Vertices: " << mdg->GetNumberOfVertices() << std::endl;
  std::cout << " Edges: " << mdg->GetNumberOfEdges() << std::endl;

  vtkNew<vtkTree> tree;
  if (!tree->CheckedShallowCopy(mdg))
  {
    std::cerr << "Could not convert graph to tree!" << std::endl;
    return EXIT_FAILURE;
  }

  std::cout << "Tree: " << std::endl;
  std::cout << " Type: " << tree->GetClassName() << std::endl;
  std::cout << " Vertices: " << tree->GetNumberOfVertices() << std::endl;
  std::cout << " Edges: " << tree->GetNumberOfEdges() << std::endl;

  vtkNew<vtkMutableDirectedGraph> mdg2;
  if (!mdg2->CheckedShallowCopy(tree))
  {
    std::cerr << "Could not convert tree to mutable directed graph!"
              << std::endl;
    return EXIT_FAILURE;
  }

  std::cout << "MDG2: " << std::endl;
  std::cout << " Type: " << mdg2->GetClassName() << std::endl;
  std::cout << " Vertices: " << mdg2->GetNumberOfVertices() << std::endl;
  std::cout << " Edges: " << mdg2->GetNumberOfEdges() << std::endl;

  return EXIT_SUCCESS;
}
