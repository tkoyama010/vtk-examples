#include <vtkDirectedGraph.h>
#include <vtkMutableDirectedGraph.h>
#include <vtkNew.h>
#include <vtkPoints.h>

int main(int, char*[])
{
  // vtkDirectedGraph is a collection of vertices along with a collection of
  // directed edges (edges that have a source and target). ShallowCopy() and
  // DeepCopy() (and CheckedShallowCopy(), CheckedDeepCopy()) accept instances
  // of vtkTree and vtkMutableDirectedGraph. vtkDirectedGraph is read-only.

  // Create a graph.
  vtkNew<vtkMutableDirectedGraph> mdg;

  // Add 4 vertices to the graph.
  vtkIdType v1 = mdg->AddVertex();
  vtkIdType v2 = mdg->AddVertex();
  vtkIdType v3 = mdg->AddVertex();
  vtkIdType v4 = mdg->AddVertex();

  // Add 3 edges to the graph.
  mdg->AddEdge(v1, v2);
  mdg->AddEdge(v1, v3);
  mdg->AddEdge(v1, v4);

  // Create 4 points - one for each vertex.
  vtkNew<vtkPoints> points;
  points->InsertNextPoint(0.0, 0.0, 0.0);
  points->InsertNextPoint(1.0, 0.0, 0.0);
  points->InsertNextPoint(0.0, 1.0, 0.0);
  points->InsertNextPoint(0.0, 0.0, 2.0);

  // add the coordinates of the points to the graph
  mdg->SetPoints(points);

  std::cout << "MDG: " << std::endl;
  std::cout << " Type: " << mdg->GetClassName() << std::endl;
  std::cout << " Vertices: " << mdg->GetNumberOfVertices() << std::endl;
  std::cout << " Edges: " << mdg->GetNumberOfEdges() << std::endl;

  vtkNew<vtkDirectedGraph> dg;
  if (!dg->CheckedShallowCopy(mdg))
  {
    std::cerr << "Could not convert mutable directed graph to directed graph!"
              << std::endl;
    return EXIT_FAILURE;
  }

  std::cout << "DG: " << std::endl;
  std::cout << " Type: " << dg->GetClassName() << std::endl;
  std::cout << " Vertices: " << dg->GetNumberOfVertices() << std::endl;
  std::cout << " Edges: " << dg->GetNumberOfEdges() << std::endl;

  return EXIT_SUCCESS;
}
