### Description

This example illustrates the use of vtkAxisActor. This is a fairly complicated object that allows extensive control over a single axis. The parameters may be tricky to set up. vtkAxisActor is usually used inside other classes,e.g. vtkCubeAxisActor.
