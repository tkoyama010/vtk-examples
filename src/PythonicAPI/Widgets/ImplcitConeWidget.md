### Description

This is an interactive implicit cone widget. It represents an infinite cone parameterized by an axis, the angle between the sides of the cone and its axis, and an origin point. Users can manipulate the widget through controls similar to the cylinder widget ones.
Its underlying cone can be used in any filter relying on implicit functions (i.e. Clip). 
