#!/usr/bin/env python3

from pathlib import Path

# noinspection PyUnresolvedReferences
import vtkmodules.vtkInteractionStyle
# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingFreeType
# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingOpenGL2
from vtkmodules.vtkCommonColor import vtkNamedColors
from vtkmodules.vtkDomainsChemistry import vtkMoleculeMapper
from vtkmodules.vtkIOChemistry import vtkCMLMoleculeReader
from vtkmodules.vtkRenderingCore import (
    vtkActor,
    vtkRenderer,
    vtkRenderWindow,
    vtkRenderWindowInteractor
)


def get_program_parameters():
    import argparse
    description = 'Read CML.'
    epilogue = '''
    '''

    parser = argparse.ArgumentParser(description=description, epilog=epilogue,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('file_name', help='The CML filename e.g. porphyrin.cml')
    args = parser.parse_args()
    return args.file_name


def main():
    colors = vtkNamedColors()

    file_name = get_program_parameters()
    fn = Path(file_name)
    if not fn.is_file():
        print(f'{fn}\nNot found.')
        return

    cml_source = vtkCMLMoleculeReader(file_name=fn)

    molmapper = vtkMoleculeMapper()
    molmapper.UseBallAndStickSettings()
    cml_source >> molmapper

    actor = vtkActor(mapper=molmapper)
    actor.property.diffuse = 0.7
    actor.property.specular = 0.5
    actor.property.specular_power = 20.0

    ren = vtkRenderer(background=colors.GetColor3d('Silver'))
    ren_win = vtkRenderWindow(size=(640, 480), window_name='ReadCML', multi_samples=0)
    ren_win.AddRenderer(ren)

    iren = vtkRenderWindowInteractor()
    iren.render_window = ren_win

    ren.AddActor(actor)

    ren_win.Render()
    ren.active_camera.Zoom(2.0)

    # Finally render the scene.
    ren_win.SetMultiSamples(0)
    ren_win.interactor.Initialize()
    ren_win.interactor.Start()


if __name__ == '__main__':
    main()
