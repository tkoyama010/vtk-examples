### Description

The vtkCubeAxesActor2D draws axes on the bounding box of the data set and labels the axes with x-y-z coordinates.

!!! seealso
    [BoundingBox](../../Utilities/BoundingBox) and [Outline](../../PolyData/Outline).
