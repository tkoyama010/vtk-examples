## Description

This example uses vtkEdgePoints to generate a set of points that lie on an isosurface. Points are generated along the edges of cells that straddle the given iso value. Unlike vtkMarchingCubes it does not generate normals at the points.
