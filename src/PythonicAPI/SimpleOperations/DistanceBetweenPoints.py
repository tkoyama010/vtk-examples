# !/usr/bin/env python3

import math

from vtkmodules.vtkCommonCore import vtkMath


def main():
    p0 = (0, 0, 0)
    p1 = (1, 1, 1)

    dist_squared = vtkMath.Distance2BetweenPoints(p0, p1)

    dist = math.sqrt(dist_squared)

    print('p0 = ', p0)
    print('p1 = ', p1)
    print('distance squared = ', dist_squared)
    print(f'distance = {dist:9.6}')


if __name__ == '__main__':
    main()
