## Description

Demonstrates the use of four renderers in two layers. The left renderers camera is independent of the right renderers camera.

There are four objects in two windows and two layers.

| Layers |  Left  |  Right   |
|:------:|:------:|:--------:|
|   0    |  cube  |   cone   |
|   1    | sphere | cylinder |

When the program first runs, the top-most layer will be the active layer, (layer 1 in this case). Objects in layer 0 will be almost transparent.

- Pressing **0** on the keyboard will let you manipulate the objects in layer 0. Objects in layer 1 will be almost transparent.
- Pressing **1** on the keyboard will let you manipulate the objects in layer 1. Objects in layer 0 will be almost transparent.

Note:

- The layer 0 background is the only visible background; backgrounds in layer 1 and subsequent layers are transparent.
- It is easy to access the renderers and actors from the interactor by iterating through the renderer and actor collections.
- In renderer collections and actor collections it is important to remember that the references are stored in last in - first out order.

!!! info
    Also see the [LayeredActors](../LayeredActors) and [TransparentBackground](../TransparentBackground) examples.
