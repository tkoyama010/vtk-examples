#!/usr/bin/env python3

# noinspection PyUnresolvedReferences
import vtkmodules.vtkInteractionStyle
# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingOpenGL2
# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingVolumeOpenGL2
from vtkmodules.vtkFiltersGeneral import vtkTimeSourceExample
from vtkmodules.vtkIOExodus import vtkExodusIIWriter


def main():
    time_source = vtkTimeSourceExample()

    fn = 'output.exii'
    exodus_writer = vtkExodusIIWriter(file_name=fn, write_all_time_steps=True)
    time_source >> exodus_writer
    exodus_writer.Write()


if __name__ == '__main__':
    main()
